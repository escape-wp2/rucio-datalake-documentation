**SNAP!** 


This documentation has become obsolete and has been replaced by the [_Virtual Research Environment_](https://vre-hub.github.io/) one. 

Thank you for your patience and contact us if you have any questions!

The VRE team

<!-- # Monitoring 

You can see the state of your replication rules on the ESCAPE Rucio [WebUI](https://escape-rucio-webui.cern.ch) platform. 

Access the ESCAPE monitoring service [here](https://monit-grafana.cern.ch/). 
If you are an external non-CERN user, follow the following steps to gain access (the option is valid as an editor or as a viewer). 'Editor' role is limited only to the experimental Playground folder dashboards.


### Get a CERN external account 
Visit the [site](https://account.cern.ch/account/Externals/), read and follow the instructions. You should have an external CERN account by now. Please verify that you can access your account by visiting [this](https://account.cern.ch/account/) and trying to sign in with the email and password that you set up. 

### Contact the team 
Please contact us at _escape-cern-ops@cern.ch_ with the following information:
* Email subject: ESCAPE Grafana - External User Registration
* Email body: Name LastName; Institute; Permission Needed (“view” or “edit”); Email that was registered at previous step. 


### Accept the invitation
You will be added as an editor or as a viewer to the ESCAPE Grafana organization. Once we add you, you will get an invitation email with the subject “ .. has invited you to join Grafana” with a link that you can click in order to accept the invitation and access the instance.

### Access the dashboards!
After you complete all the steps you will be able to access the [Grafana instance](https://monit-grafana.cern.ch/d/cHBQ2NjWz/escape-home?orgId=51).

### Get started

In order to edit, view or analyze data from ESCAPE Grafana you first need to get familiar with the core technologies that are used. Below you will find an non-exhaustive list of some documentation to get you started.

Grafana:



* https://grafana.com/docs/grafana/latest/guides/getting_started/
* https://grafana.com/docs/grafana/latest/features/panels/panels/
* https://grafana.com/docs/grafana/latest/features/dashboard/dashboards/
* https://grafana.com/docs/grafana/latest/reference/templating/
* https://grafana.com/docs/grafana/latest/features/datasources/


Elasticsearch



* https://grafana.com/docs/grafana/latest/features/datasources/elasticsearch/

 -->
