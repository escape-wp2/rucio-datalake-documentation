**SNAP!** 


This documentation has become obsolete and has been replaced by the [_Virtual Research Environment_](https://vre-hub.github.io/) one. 

Thank you for your patience and contact us if you have any questions!

The VRE team

<!-- A series of facts and trasfer knowledge tips to navigate, deploy and maintain the Data Lake infrastructure. 

# The Data Lake Infrastructure (**under development**)
The Data Lake infrastructure takes advantage of various concepts and technologies. In order to be able to maintain and deploy the service you should be more or less familiar with:

- Containers and Virtual Machines
- K8s
- Helm Charts
- Flux integration, GitLab/Github
- Puppet for caching 

The Data Lake Architechture is organised in the following way (a graph is worth more than a thousand words):

 <!-- <img src="images/DLarchitechture.png" width="500"> -->


<!-- 1. Federated data infrastructure
- Multiple storage/protocol technologies
- Combination of HEP-specific services and industry standards
- QoS and file transitions, distributed redundancy and data policies
2. Data transfer
- Rucio is the Data orchestrator for policy-driven data management
- FTS3 is the Middleware a reliable large scale file transfer service
- GFAL2 is the Grid file access library with multi-protocol access
3. Identity and Access Management
4. CRIC Information Catalogue
5. perfSONAR boxes deployed in the OSG/WLCG network
6. Gitlab CI to maintain infrastructure up to date

The DL is based on the Rucio architechture, depicted in the following image, organised in [containers](https://github.com/rucio/containers) and managed through [helm charts](https://github.com/rucio/helm-charts). 

 <!-- <img src="images/rucio_archit.png" width="500" center> -->
<!-- 
The architechture is based on the [ESCAPE DIOS github ](https://github.com/ESCAPE-WP2) repo and on the [WP2 gitlab](https://gitlab.cern.ch/escape-wp2) one. 

## Computing resources
- Openstack VM with 40 GB RAM and 20 CPU cores. 
- [Puppet](https://configdocs.web.cern.ch/index.html) group name: escapewp2c 
- RSE are the Rucio Storage Elements, example to create RSE:

```console
$ rucio-admin rse set-attribute --key verify_checksum --value false --rse LAPP-CAN1 
$ rucio upload new --rse LAPP-CAN1 --scope scope_1 --name uploadasrucioclient
$ gfal-ls xroot://lapp-testse01.in2p3.fr:1094/dteam/afkiaras/escape_wp2_datalake/testing/LAPP-CAN1/scope_1/48/26
```

## Rucio deployment - start learning 

To get started, here a Rucio containerised version of [deployment](http://rucio.cern.ch/documentation/setting-up-demo/), where you can chose the basic option, or whether to also connect to a database and monitoring services. 

Change ports in docker-compose.yml to :
```console
ports:
    - "8443:443"
    - "5432:5432"
    - "8080:80"
``` -->
<!-- In order for everybody to access the VM with server on it:
```console
sudo firewall-cmd --zone=public --add-port=443/tcp --permanent
sudo firewall-cmd --reload
```
To interact with the server remotely from lxplus or from your VM:
```console
curl --insecure -X GET https://<name_of_your_VM>.cern.ch:8443/ping
```
## The ESCAPE DataBase

You can connect to the ESCAPE DB through aiadm (`ssh <escape_username>@aiadm.cern.ch`). To do so, create a [Two-Factor Authentication](https://security.web.cern.ch/recommendations/en/2FA.shtml) first, and initiate [kerberos](https://linux.web.cern.ch/docs/kerberos-access/) with (mind the capital letters!) `kinit -f <escape_username>@CERN.CH`. 
In order to view the keys, and then to explore each $KEY_NAME content:
```console
tbag showkeys --hg escapewp2c
tbag show --hg escapewp2c $KEY_NAME
```
SQLite DB migration [here](https://dbod.web.cern.ch/#/pages/dashboard). 

To upload a dataset, chose a <scope_name>, which should be in the format **EXPERIMENT_INSTITUTE_project**, an arbitrary <dataset_name> and have your <file_name> ready corresponding to which local files you want to upload.
```console 

rucio add-dataset <scope_name>:<dataset_name>
rucio attach <scope_name>:<dataset_name> <scope_name>:<file_name>
rucio list-files <scope_name>:<dataset_name>
```

Your rucio configuration file, one you have registered for an account, should be in `/opt/rucio/etc/config.cfg`. 

## Testing 

Continuous testing is crucial to consolidate the infrastructure. 
GFAL funcional testing for upload, download and deletion. 
CRIC automatically fetches the RSE (storage element) configuration before each run. 


## Monitoring 

The basic GFAL operations per RSE, TPC transfers between endpoints and Rucio specific activities are being monitored with Elasticsearch and Kibana. 

 <!-- <img src="images/monitoring.png" width="500" center> -->
<!--  
There exists an Elasticsearch cluster for monitoring with 2 nodes running on the same machine. 

Elasticsearch image: `docker pull docker.elastic.co/elasticsearch/elasticsearch:6.6.1`
Kibana image: `docker pull docker.elastic.co/kibana/kibana:6.6.1`

The Kibana instance points to the ES cluster (machine name is escape-wp2-kibana-01.cern.ch). 
Running on CERN-Monit services. For partner institutions interested in deploying on their infrastructure, a dev monitoring stack is being run in parallel.

# CI/CD

In order to keep the K8s DL cluster in sync with the Git updates, the configuration is automatically updated with [Flux](https://fluxcd.io/docs/). 

# Authenticating users and certificates 
Authorization in CERN's present Grid applications is based on the concept of Virtual
Organizations (VO). VOMS (Virtual Organization Membership Service) provides information on the user's
relationship with his VO: his groups, roles and capabilities. It is a system to classify users
that are part of a VO on the base of a set of attributes that will be granted to them upon
request and to include that information inside globus-compatible proxy certificates
(VOMS extensions). The ESCAPE VOMS are specified [here](https://github.com/ESCAPE-WP2/Rucio-Client-Containers/blob/master/rucio-client-container/Dockerfile#L8) and the connection is achieved through INFN's IAM (Identity and Access Management). 

# DLaaS Milestone 4 - Integrate XCache with DLaaS

## Description

This milestone seeks to integrate an XCache instance with the ESCAPE Rucio and EOS eulake. Rucio clients retrieving files from the `EULAKE-1` RSE should retrieve the files through XCache. -->

<!-- ## Steps

### Configuring XCache Authfile to allow access to EOS eulake

Modify the Authfile in the escapewp2c Puppet hostgroup to include `eulakepaths`

- Authfile repo: https://gitlab.cern.ch/ai/it-puppet-hostgroup-escapewp2c/-/blob/xcache_test/code/templates/xcache/etc__xrootd__Authfile.erb
- Add this line to define the allowed paths:

```
t eulakepaths   /root:/eoseulake.cern.ch:1094/    lr
```

- And also modify this line to include `eulakepaths`:

```
s escapevo escapepaths mockdatapaths eulakepaths
```

Now, clients access 
There exists an Elasticsearch cluster for monitoring with 2 nodes running on the same machine. 

Elasticsearch image: `docker pull docker.elastic.co/elasticsearch/elasticsearch:6.6.1`
Kibana image: `docker pull docker.elastic.co/kibana/kibana:6.6.1`

The Kibana instance points to the ES cluster (machine name is escape-wp2-kibana-01.cern.ch). 
Running on CERN-Monit services. For partner institutions interested in deploying on their infrastructure, a dev monitoring stack is being run in parallel.

# CI/CD

In order to keep the K8s DL cluster in sync with the Git updates, the configuration is automatically updated with [Flux](https://fluxcd.io/docs/). 

# Authenticating users and certificates 
Authorization in CERN's present Grid applications is based on the concept of Virtual
Organizations (VO). VOMS (Virtual Organization Membership Service) provides information on the user's
relationship with his VO: his groups, roles and capabilities. It is a system to classify users
that are part of a VO on the base of a set of attributes that will be granted to them upon
request and to include that information inside globus-compatible proxy certificates
(VOMS extensions). The ESCAPE VOMS are specified [here](https://github.com/ESCAPE-WP2/Rucio-Client-Containers/blob/master/rucio-client-container/Dockerfile#L8) and the connection is achieved through INFN's IAM (Identity and Access Management). 

# DLaaS Milestone 4 - Integrate XCache with DLaaS

## Description

This ming with a gsi proxy certificate having the `escape` VO will be able to connect to EOS eulake.

### Configuring EULAKE-1 RSE in Rucio to recognize XCache

Rucio has a (not yet documented) feature that appends an XRootD proxy prefix to the Replica PFN if:

1. The XCache URL is registered to Rucio to a specific site name
2. The protocol is `root`
3. The client is accessing Rucio with a site name that matches the one configured in (1).

To configure Rucio to recognize the XCache instance, run the following in `rucio-admin` CLI:

```sh
rucio-admin config set --section 'root-proxy-internal' --option 'xcache_test' --value 'xcache-redirector.cern.ch'
```

Where `xcache_test` is the site name and `xcache-redirector.cern.ch` is the hostport of the XCache instance. Note that there is no `root://` prefix.

### Verifying that Rucio has Recognized the XCache

If everything is set up, use the following command to see if the prefix is prepended:

```sh
SITE_NAME=xcache_test rucio list-file-replicas ESCAPE_CERN_TEAM-testing:thesmall-2.dat --protocols root,gsiftp
```

The response should be:

```
+--------------------------+----------------+------------+-----------+---------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SCOPE                    | NAME           | FILESIZE   | ADLER32   | RSE: REPLICA                                                                                                                                                  |
|--------------------------+----------------+------------+-----------+---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ESCAPE_CERN_TEAM-testing | thesmall-2.dat | 1.049 MB   | 00f00001  | EULAKE-1: gsiftp://eulakeftp.cern.ch:2811/eos/eulake/tests/rucio_test/eulake_1/ESCAPE_CERN_TEAM-testing/7f/06/thesmall-2.dat                                  |
| ESCAPE_CERN_TEAM-testing | thesmall-2.dat | 1.049 MB   | 00f00001  | EULAKE-1: root://xcache-redirector.cern.ch//root://eoseulake.cern.ch:1094//eos/eulake/tests/rucio_test/eulake_1/ESCAPE_CERN_TEAM-testing/7f/06/thesmall-2.dat |
+--------------------------+----------------+------------+-----------+---------------------------------------------------------------------------------------------------------------------------------------------------------------+
```

Note that only `root` protocol is prepended with the XCache prefix.

