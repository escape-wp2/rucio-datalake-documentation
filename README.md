**SNAP!** 

This documentation has become obsolete and has been replaced by the [_Virtual Research Environment_](https://vre-hub.github.io/) one. 

Thank you for your patience and contact us if you have any questions!

The VRE team